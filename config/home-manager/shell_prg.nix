{ config, pkgs, ... }:
let
  vscode-for-tig = import ./tig.nix { inherit pkgs; };
in {
  programs.zsh = {
    enable = true;
    # https://github.com/NixOS/nix/issues/5445#issuecomment-954273357
    enableCompletion = false;
    initExtraBeforeCompInit = ''
      ABBR_DEFAULT_BINDINGS=0

      function list() {
          emulate -L zsh
          ls --color -F
      }
      chpwd_functions=(''${chpwd_functions[@]} "list")

      my-accept-line () {
        # https://stackoverflow.com/questions/30169090/zsh-behavior-on-enter
        if [ ''${#''${(z)BUFFER}} -eq 0 ]
        then
          echo
          if git rev-parse --git-dir > /dev/null 2>&1
          then git status -sb
          else ls --color -F
          fi
        fi
        zle accept-line # See man zshzle
      }
      zle -N my-accept-line
      bindkey '^M' my-accept-line # rebind Enter

      function prev() {
        PREV=$(fc -lrn | head -n 1)
        sh -c "pet new `printf %q "$PREV"`"
      }

      function pet-select() {
        BUFFER=$(pet search --query "$LBUFFER")
        CURSOR=$#BUFFER
        # zle redisplay
      }
      zle -N pet-select
      stty -ixon
      bindkey '^s' pet-select

      if [ -n "''${commands[fzf-share]}" ]; then
        source "$(fzf-share)/key-bindings.zsh"
        source "$(fzf-share)/completion.zsh"
      fi
    '';
    initExtra = ''
      ##
      # Session variables
      export TIG_EDITOR="${vscode-for-tig}/bin/vscode-for-tig"
      export EDITOR="vi";
      export LESS="-RF";
      export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s";
      export FZF_DEFAULT_COMMAND=fd
      export FZF_TMUX=1
      export FZF_TMUX_OPTS="-p 80%"
      export FZF_TMUX_HEIGHT=25
      export BAT_STYLE=plain
      export GOPATH="$HOME/.go";
      export PATH="$HOME/.npm-global/bin:$GOPATH/bin:$PATH";
      export FZF_ALT_C_OPTS="--preview 'eza -T {} | head -100'"
      export FZF_CTRL_T_OPTS="--preview 'bat --color=always --style=numbers {} 2> /dev/null || eza -F {} |head -100'"


      ABBR_QUIET=1
      bindkey " " abbr-expand-and-space
      abbr -S -g gp="grep"
      abbr -S bl="bundle"
      abbr -S dk="docker"
      abbr -S e="$EDITOR"
      abbr -S g="git"
      abbr -S -="cd -"
      abbr import-aliases
      abbr import-git-aliases
      eval "$(starship init zsh)"
      eval "$(~/.nix-profile/bin/mise activate zsh)"
    '';
    shellAliases = {
      cat = "bat --paging=never";
      diff = "diff --color=auto";
      grep = "grep --color=auto";
      fzf = "fzf-tmux";
      hq = "cd $(ghq root)/$(ghq list | fzf --preview 'eza -1 $(ghq root)/{}')";
      ip = "ip -color=auto";
      ln = "ln -v";
      ls = "eza";
      l = "ls --group-directories-first --classify auto --reverse --sort=accessed --git-ignore";
      ll = "ls --all --long --git";
      pbcopy = "wl-copy";
      t = "tig";
    };
    history = {
      size = 10000;
      path = "${config.xdg.dataHome}/zsh/history";
    };
    zplug = {
      enable = true;
      plugins = [
        { name = "zsh-users/zsh-autosuggestions"; }
        { name = "Tarrasch/zsh-bd"; }
        { name = "agkozak/zsh-z"; }
        { name = "hlissner/zsh-autopair"; }
        { name = "chisui/zsh-nix-shell"; }
        { name = "olets/zsh-abbr"; }
        { name = "junegunn/fzf"; }
        { name = "plugins/colored-man-pages"; tags = [ "from:oh-my-zsh" ]; }
        { name = "plugins/ssh-agent"; tags = [ "from:oh-my-zsh" ]; }
        { name = "plugins/sudo"; tags = [ "from:oh-my-zsh" ]; }
      ];
    };
  };
}
