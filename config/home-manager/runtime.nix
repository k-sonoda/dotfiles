{ config, pkgs, lib, ... }: {
  imports = [ ];
  home.packages = with pkgs; [
    lua-language-server
    # alloy6
    # jdk17

    # node2nix
    nodePackages.dockerfile-language-server-nodejs
    nodePackages.npm
    nodePackages.typescript-language-server
    nodePackages.vscode-langservers-extracted
    nodePackages.yaml-language-server
    nodejs
  ];
}
