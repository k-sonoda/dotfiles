require("nvim-treesitter.configs").setup({
	sync_install = false,
	ignore_install = {},
	highlight = {
		enable = true,
		disable = {},

		-- Using this option may slow down your editor, and you may see some duplicate highlights.
		additional_vim_regex_highlighting = false,
	},
	textsubjects = {
		enable = true,
		prev_selection = 'V', -- (Optional) keymap to select the previous selection
		keymaps = {
			['v'] = 'textsubjects-smart',
		},
	},
})
