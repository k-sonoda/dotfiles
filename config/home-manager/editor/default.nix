{ pkgs, lib, ... }:
let
  fromGitHub = ref: repo: pkgs.vimUtils.buildVimPlugin {
    pname = "${lib.strings.sanitizeDerivationName repo}";
    version = ref;
    src = builtins.fetchGit {
      url = "https://github.com/${repo}.git";
      ref = ref;
    };
  };
in
{
  programs.neovim =
    {
      enable = true;
      plugins =
        (with pkgs.vimPlugins; [
          # Coding
          luasnip # https://github.com/L3MON4D3/LuaSnip
          # TODO: # friendly-snippets # https://github.com/rafamadriz/friendly-snippets/
          nvim-cmp # https://github.com/hrsh7th/nvim-cmp
          cmp-nvim-lsp # https://github.com/hrsh7th/cmp-nvim-lsp
          cmp-buffer # https://github.com/hrsh7th/cmp-buffer
          cmp-path # https://github.com/hrsh7th/cmp-path
          # TODO: # cmp_luasnip # https://github.com/saadparwaiz1/cmp_luasnip/
          {
            plugin = mini-nvim; # https://github.com/echasnovski/mini.nvim
            type = "lua";
            config = ''
              vim.cmd.colorscheme "minischeme"
              require('mini.ai').setup()
              require('mini.bufremove').setup()
              require('mini.comment').setup()
              require('mini.cursorword').setup()
              require('mini.indentscope').setup({
                draw = {
                  animation = require('mini.indentscope').gen_animation.none()
                }
              })
              require('mini.jump').setup()
              require('mini.pairs').setup()
              require('mini.statusline').setup()
              require('mini.surround').setup()
              require('mini.tabline').setup()
            '';
          }
          nvim-ts-context-commentstring # https://github.com/JoosepAlviste/nvim-ts-context-commentstring
          # ---
          cmp-cmdline # https://github.com/hrsh7th/cmp-cmdline
          cmp-git # https://github.com/petertriho/cmp-git
          cmp-spell # https://github.com/f3fora/cmp-spell

          # Editor
          {
            plugin = neo-tree-nvim; # https://github.com/nvim-neo-tree/neo-tree.nvim
            type = "lua";
            config = ''
              require("neo-tree").setup()

              vim.api.nvim_set_keymap(
                'n', '<Leader>ft', ":Neotree toggle<CR>", {}
              )
            '';
          }
          # TODO: # nvim-spectre # https://github.com/nvim-pack/nvim-spectre/
          {
            plugin = telescope-nvim; # https://github.com/nvim-telescope/telescope.nvim
            type = "lua";
            config = lib.readFile "${./telescope-nvim.lua}";
          }
          # TODO: # which-key-nvim # https://github.com/folke/which-key.nvim/
          {
            plugin = gitsigns-nvim; # https://github.com/lewis6991/gitsigns.nvim
            type = "lua";
            config = ''
              require("gitsigns").setup()
            '';
          }
          trouble-nvim # https://github.com/folke/trouble.nvim/
          # TODO: todo-comments-nvim # https://github.com/folke/todo-comments.nvim/
          # ---
          telescope-vim-bookmarks-nvim # https://github.com/tom-anders/telescope-vim-bookmarks.nvim
          {
            plugin = telescope-undo-nvim; # https://github.com/debugloop/telescope-undo.nvim
            type = "lua";
            config = ''
              require("telescope").load_extension("undo")
              vim.keymap.set("n", "<leader><leader>", "<cmd>Telescope undo<cr>")
            '';
          }
          telescope-frecency-nvim

          # LSP
          {
            plugin = nvim-lspconfig; # https://github.com/neovim/nvim-lspconfig
            type = "lua";
            config = lib.readFile "${./nvim-lspconfig.lua}";
          }
          neoconf-nvim # https://github.com/folke/neoconf.nvim/
          # TODO: neodev-nvim # https://github.com/folke/neodev.nvim/
          # ---
          lsp-zero-nvim # https://github.com/VonHeikemen/lsp-zero.nvim
          lspkind-nvim # https://github.com/onsails/lspkind.nvim
          cmp-nvim-lsp-document-symbol # https://github.com/hrsh7th/cmp-nvim-lsp-document-symbol
          cmp-nvim-lsp-signature-help # https://github.com/hrsh7th/cmp-nvim-lsp-signature-help
          {
            plugin = symbols-outline-nvim; # https://github.com/simrat39/symbols-outline.nvim
            type = "lua";
            config = lib.readFile "${./symbols-outline-nvim.lua}";
          }

          # TreeSitter
          {
            # https://github.com/nvim-treesitter/nvim-treesitter
            plugin = nvim-treesitter.withAllGrammars;
            type = "lua";
            config = lib.readFile "${./nvim-treesitter.lua}";
          }
          # ---
          nvim-treesitter-textsubjects
          {
            plugin = nvim-ts-autotag; # https://github.com/windwp/nvim-ts-autotag
            type = "lua";
            config = ''
              require('nvim-ts-autotag').setup()
            '';
          }

          # UI
          # nvim-web-devicons # https://github.com/nvim-tree/nvim-web-devicons
          # ---
          {
            plugin = nvim-treesitter-context; # https://github.com/nvim-treesitter/nvim-treesitter-context
            type = "lua";
            config = lib.readFile "${./nvim-treesitter-context.lua}";
          }
          {
            plugin = nvim-scrollview; # https://github.com/dstein64/nvim-scrollview
            type = "lua";
            config = ''
              require('scrollview').setup({
                current_only = true,
              })
            '';
          }
          {
            plugin = fidget-nvim; # https://github.com/j-hui/fidget.nvim
            type = "lua";
            config = ''
              require"fidget".setup{}
            '';
          }

          # Util
          plenary-nvim # https://github.com/nvim-lua/plenary.nvim
        ] ++ [
          # Tier 2
          nvim-dap # https://github.com/mfussenegger/nvim-dap
          nvim-bqf # https://github.com/kevinhwang91/nvim-bqf
          {
            plugin = project-nvim; # https://github.com/airblade/vim-rooter
            type = "lua";
            config = ''
              require("project_nvim").setup()
            '';
          }
          {
            plugin = refactoring-nvim; # https://github.com/ThePrimeagen/refactoring.nvim
            type = "lua";
            config = ''
              require('refactoring').setup({})
            '';
          }
          vim-bracketed-paste # https://github.com/ConradIrwin/vim-bracketed-paste

          # Motion
          vim-fetch # https://github.com/wsdjeg/vim-fetch
          vim-lastplace # https://github.com/farmergreg/vim-lastplace/
          {
            plugin = vim-bookmarks; # https://github.com/MattesGroeger/vim-bookmarks
            config = lib.readFile "${./vim-bookmarks.vim}";
          }
          vim-wordmotion # https://github.com/chaoren/vim-wordmotion
        ]);
      extraConfig = ''
        source ${./globals.lua}
        source ${./mappings.vim}
      '';

      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;
    };

}

