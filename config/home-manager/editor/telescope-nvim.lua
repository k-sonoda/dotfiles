local telescope = require("telescope")
telescope.setup({
	extensions = {}
})
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<C-p>', builtin.find_files, {})
vim.keymap.set('n', '<leader>/', builtin.live_grep, {})
vim.keymap.set('n', '<leader>bb', builtin.buffers, {})
-- vim.keymap.set('n', '<leader><C-p>', builtin.oldfiles, {})
vim.keymap.set('n', '<leader>Sg', builtin.git_status, {})

telescope.load_extension("frecency")
vim.api.nvim_set_keymap(
	"n", "<leader>fr",
	"<Cmd>lua require('telescope').extensions.frecency.frecency({ workspace = 'CWD' })<CR>",
	{ noremap = true, silent = true }
)

telescope.load_extension('vim_bookmarks')
vim.api.nvim_set_keymap(
	'n', '<F5>', "<cmd> lua require('telescope').extensions.vim_bookmarks.all()<CR>", {}
)
