vim.opt.history = 50
vim.opt.updatetime = 300 -- longer updatetime leads to noticeable delays

vim.opt.modelines = 0    -- Disable modelines as a security precaution
vim.opt.modeline = false

vim.opt.backup = false -- Some servers have issues with backup files
vim.opt.writebackup = false
-- http://robots.thoughtbot.com/post/18739402579/global-gitignore#comment-458413287
vim.opt.swapfile = false
vim.opt.undofile = true
vim.opt.autowrite = true -- Automatically :write before running commands
vim.opt.confirm = true

vim.opt.wildmode = { 'list:longest', 'list:full' }
vim.opt.wildignore:append { '*/tmp/*', '*.so', '*.swp', '*.zip', '*.pyc', '*.db', '*.sqlite' }
vim.opt.completeopt = { 'menu', 'menuone', 'noselect', 'longest' }

-- Set spellfile to location that is guaranteed to exist, can be symlinked to
-- Dropbox or kept in Git and managed outside of thoughtbot/dotfiles using rcm.
vim.opt.spell = true
vim.opt.spelllang = { 'en_us' }
vim.opt.spellfile = '$HOME/.vim-spell-en.utf-8.add'
vim.opt.dictionary = '/usr/share/dict/words'
vim.opt.complete:append('kspell') -- Autocomplete with dictionary words when spell check is on

vim.opt.joinspaces = false        -- Use one space, not two, after punctuation.
vim.opt.clipboard:append('unnamedplus')

-- Visual --
-- Display extra whitespace
vim.opt.list = true
vim.opt.listchars = { trail = "•", nbsp = "␣" }
vim.opt.termguicolors = true

vim.opt.diffopt:append('vertical') -- Always use vertical diffs
-- Numbers
vim.opt.number = true
vim.opt.relativenumber = true
-- Searching
vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.opt.mouse = 'a'
vim.opt.showtabline = 2
vim.opt.shortmess:append('c') -- Don't pass messages to |ins-completion-menu|.
-- otherwise it would shift the text each time diagnostics appear/become resolved.
vim.opt.signcolumn = 'yes'
vim.opt.scrolloff = 1
vim.opt.cursorline = true
vim.opt.textwidth = 80 --  https://prettier.io/docs/en/options.html#print-width
vim.opt.colorcolumn = '+1'
-- Open new split panes to right and bottom, which feels more natural
vim.opt.splitbelow = true
vim.opt.splitright = true

if vim.fn.executable('rg') == 1 then
	vim.opt.grepprg = 'rg --vimgrep'
	vim.cmd([[
		let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
		command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)
]])
end

vim.g.is_posix = 1 -- See ft-posix-syntax
vim.api.nvim_set_var('jasegment#model', 'knbc_bunsetu')

vim.cmd([[
	autocmd InsertEnter * :set norelativenumber
	autocmd InsertLeave * :set relativenumber
	autocmd QuickfixCmdPost grep copen
	autocmd TermOpen term://* startinsert
]])

vim.api.nvim_command [[autocmd! User LspDiagnosticsChanged lua update_diagnostics_loclist()]]
vim.api.nvim_create_autocmd('DiagnosticChanged', {
	callback = function()
		-- -- local diagnostics = args.data.diagnostics
		-- vim.print(diagnostics)

		vim.diagnostic.setloclist({ open = false })
	end,
})

vim.g.mapleader = " "
vim.api.nvim_set_keymap('n', '<Leader><Tab>', '<C-^>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>bNn', ':tabnew<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>bd', ':bd<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>sc', ':noh<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>el', ':lopen<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', 'g/', ':<C-u>Migemo<CR>', { noremap = true })
vim.api.nvim_set_keymap(
	'n', "<Leader>'",
	":call termopen('zsh', {'cwd': expand('%:p:h')})<CR>",
	{ noremap = true, silent = true }
)

vim.api.nvim_set_keymap('v', '>', '>gv', { noremap = true })
vim.api.nvim_set_keymap('v', '<', '<gv', { noremap = true })
vim.api.nvim_set_keymap('v', 'p', '"_dP', { noremap = true })
