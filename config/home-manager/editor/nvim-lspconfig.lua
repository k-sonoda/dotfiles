require("neoconf").setup({})
local lsp = require('lsp-zero').preset(
	{
		float_border = 'rounded',
		call_servers = 'global',
		configure_diagnostics = true,
		setup_servers_on_start = false,
		set_lsp_keymaps = {
			preserve_mappings = false,
			omit = { '<F2>', '<F3>', '<F4>' },
		},
		manage_nvim_cmp = false,
	})

lsp.on_attach(function(_, bufnr)
	lsp.default_keymaps({ buffer = bufnr })
	lsp.buffer_autoformat()
	local opts = { buffer = bufnr }
	local bind = vim.keymap.set
	bind('n', 'gr', '<cmd>Telescope lsp_references<cr>', opts)
	bind('n', '<F6>', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
	bind({ 'n', 'x' }, '<F7>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
	bind('n', '<F8>', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)
end)


lsp.setup_servers({
	"clangd",
	"cssls",
	"dockerls",
	"gopls",
	"html",
	"jsonls",
	"lua_ls",
	"marksman",
	"nil_ls",
	"rust_analyzer",
	"sqlls",
	"ts_ls",
	"yamlls"
})
require('lspconfig').ruby_lsp.setup {}

local cmp = require('cmp')
local cmp_action = lsp.cmp_action()
cmp.setup({
	sources = cmp.config.sources({
		{ name = "nvim_lsp" },
		{ name = 'nvim_lsp_signature_help' }
	}, {
		{ name = "buffer", keyword_length = 3 },
		{
			name = 'spell',
			keyword_length = 5,
			option = {
				keep_all_entries = false,
				enable_in_context = function()
					return true
				end,
			},
		},
	}),
	mapping = lsp.defaults.cmp_mappings({
		['<C-f>'] = cmp_action.luasnip_jump_forward(),
		['<C-b>'] = cmp_action.luasnip_jump_backward(),
		['<CR>'] = cmp.mapping.confirm({ select = true }),
		['<C-u>'] = cmp.mapping.scroll_docs(-4),
		['<C-d>'] = cmp.mapping.scroll_docs(4),
	}),
	formatting = {
		fields = { 'menu', 'abbr', 'kind' },
		format = require('lspkind').cmp_format({})
	},
	window = {
		completion = cmp.config.window.bordered(),
		documentation = cmp.config.window.bordered(),
	},

	snippet = {
		-- REQUIRED - you must specify a snippet engine
		expand = function(args)
			require('luasnip').lsp_expand(args.body)
		end,
	},
})

cmp.setup.filetype("gitcommit", {
	sources = cmp.config.sources({
		{ name = "cmp_git" },
	}, {
		{ name = "buffer" },
	}),
})

cmp.setup.cmdline("/", {
	mapping = cmp.mapping.preset.cmdline(),
	sources = cmp.config.sources({
		{ name = 'nvim_lsp_document_symbol' }
	}, {
		{ name = "buffer" },
	})
})

cmp.setup.cmdline(":", {
	mapping = cmp.mapping.preset.cmdline(),
	sources = cmp.config.sources({
		{ name = "path" },
	}, {
		{ name = "cmdline" },
	}),
})
lsp.setup()
