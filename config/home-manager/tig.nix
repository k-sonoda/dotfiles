{ pkgs, ... }:
pkgs.stdenv.mkDerivation {
  name = "vscode-for-tig";
  src = pkgs.fetchFromGitHub {
    owner = "qq88976321";
    repo = "vscode-for-tig";
    rev = "refs/tags/v0.1.0";
    sha256 = "GIs1Gty9G7dIGVOv/X4gu9fimgGbljO2ZpjZiCEm6kE=";
  };

  installPhase = ''
    mkdir -p $out/bin
    cp $src/vscode-for-tig $out/bin/
  '';
}
