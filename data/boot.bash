#!/usr/bin/env bash
set -euo pipefail
set -vx

declare -r profileId=$(
  gsettings get org.gnome.Terminal.ProfilesList default |
    tr -d \'
)

gsettings set \
  org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profileId}/ \
  use-system-font "false"

gsettings set \
  org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${profileId}/ \
  font "Ubuntu Mono 20"

cat ./dconf_dump.ini | dconf load /

##
# Install
sudo apt-get install -yq \
    ca-certificates \
    curl \
    gnupg \

    # apt-transport-https \

sudo install -m 0755 -d /etc/apt/keyrings

# chrome
curl -fsSL https://dl-ssl.google.com/linux/linux_signing_key.pub |
  sudo gpg --yes --dearmor -o /usr/share/keyrings/google-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) \
    signed-by=/usr/share/keyrings/google-archive-keyring.gpg] \
    http://dl.google.com/linux/chrome/deb/ \
    stable main" |
  sudo tee /etc/apt/sources.list.d/google-chrome.list > /dev/null

# vscode
curl -fsSL https://packages.microsoft.com/keys/microsoft.asc |
  sudo gpg --yes --dearmor -o /usr/share/keyrings/packages.microsoft.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) \
    signed-by=/usr/share/keyrings/packages.microsoft.gpg] \
    https://packages.microsoft.com/repos/code \
    stable main" |
  sudo tee /etc/apt/sources.list.d/vscode.list > /dev/null

# docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg |
  sudo gpg --yes --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) \
    signed-by=/etc/apt/keyrings/docker.gpg] \
    https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" |
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

echo \
  "deb \
    https://ppa.launchpadcontent.net/linrunner/tlp/ubuntu \
    $(lsb_release -cs) main" |
  sudo tee /etc/apt/sources.list.d/linrunner-tlp.list > /dev/null

sudo apt-get update -qq
DEBIAN_FRONTEND=noninteractive \
    xargs sudo apt-get install -yq --no-install-recommends < Aptfile

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install -y flathub com.github.d4nj1.tlpui
flatpak install -y flathub com.mattjakeman.ExtensionManager

echo '
START_CHARGE_THRESH_BAT0=70
STOP_CHARGE_THRESH_BAT0=90
' | sudo tee /etc/tlp.d/01-battery-care.conf

sudo systemctl mask power-profiles-daemon.service
sudo systemctl enable tlp.service

sudo apt-file update
sudo etckeeper init

# diff-highlight
(
    dir=/usr/share/doc/git/
    target=/usr/local/bin/
    cd ${dir}contrib/diff-highlight/ &&
        sudo make &&
        sudo chmod +x diff-highlight &&
        sudo mv diff-highlight ${target}
)

##
# Config
grep -q GTK_IM_MODULE ~/.pam_environment || echo 'IM_MODULE not found'
im-config -n fcitx5
mkdir -p ~/.config/autostart/
ln -fs /usr/share/applications/org.fcitx.Fcitx5.desktop ~/.config/autostart/

# ykman
sudo systemctl enable pcscd.service

# https://nixos.org/manual/nix/stable/installation/uninstall
# sudo rm /etc/*.backup-before-nix /etc/profile.d/nix.sh.backup-before-nix
sh <(curl -L https://nixos.org/nix/install) --daemon
# . ~/.nix-profile/etc/profile.d/nix.sh
nix-channel --add https://github.com/guibou/nixGL/archive/main.tar.gz nixgl
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update

feats='nix-command flakes'
nix --extra-experimental-features "$feats" run home-manager/release-23.11 -- init --switch
nix --extra-experimental-features "$feats" profile list | grep -q cachix || \
  nix --extra-experimental-features "$feats" profile install nixpkgs#cachix --priority 4
cachix use devenv

# https://qiita.com/Broccolingual/items/d64406a56864ba3888f7
cat << 'EOS' | sudo tee /etc/udev/rules.d/90-nfc.rules
SUBSYSTEMS=="usb", ACTION=="add", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="06c3", GROUP="plugdev"
EOS
cat << 'EOS' | sudo tee /etc/modprobe.d/blacklist-nfc.conf
blacklist port100
EOS

sudo udevadm control -R

sudo sed -i 's/\(XKBOPTIONS=\"\)\S*\"/\1caps:escape"/g' /etc/default/keyboard
grep -q 'NotShowIn=' /etc/xdg/autostart/org.gnome.Evolution-alarm-notify.desktop ||
echo 'NotShowIn=GNOME;' | sudo tee -a /etc/xdg/autostart/org.gnome.Evolution-alarm-notify.desktop
sudo sed -i 's/\(GRUB_CMDLINE_LINUX_DEFAULT=\).*/\1""/' /etc/default/grub
sudo update-grub

sudo usermod $USER -aG i2c
sudo adduser $USER docker
sudo adduser $USER kvm
getent group libvirt || sudo groupadd --system libvirt
sudo adduser $USER libvirt

git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it
~/.bash_it/install.sh

curl https://mise.jdx.dev/install.sh | sh

