# Links

- <https://github.com/yuru7/PlemolJP/releases/latest>
- <https://extensions.gnome.org/extension/3733/tiling-assistant/>
- <https://github.com/arosh/ublacklist-stackoverflow-translation>

```sh
dconf dump / |awk '/wm\/keybindings]/,/^$/' > dconf_dump.ini
ls ~/.config/google-chrome/Default/Extensions |
 grep -v Temp > config_google-chrome_Default_Extensions.txt
code --list-extensions > extensions.txt
```

```sh
cat extensions.txt | xargs -L 1 code --install-extension
```

set `~/.config/autostart`

# yaskkserv

<https://github.com/wachikun/yaskkserv2#install>

```sh
bash -x yaskkserv.sh
```

# Wine

```sh
winetricks apps list | grep kindle
winetricks kindle
```

# display manager

<https://wiki.archlinux.org/title/SDDM#Autologin>
