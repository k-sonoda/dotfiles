#!/bin/bash -eu
set -o pipefail
# set -x

declare -r token=$(secret-tool lookup slack token)

api(){
    declare -r method=$1
    declare -r path=$2
    declare -r query=${3:-}
    curl --silent --show-error --fail \
      -X $method "https://slack.com/api/${path}${query}" \
      -H "Authorization: Bearer ${token}" |
    jq --exit-status '.ok'
}
api POST dnd.endSnooze || true
api GET dnd.setSnooze ?num_minutes=25 && /usr/bin/notify-send -i gnome-info "Restart Snooze"
